# README для GitLab CI/CD конвейера

## Этапы конвейера

Конвейер состоит из одного этапа:

1. **build**: Пакетирование и загрузка Helm чартов в GitLab Package Registry.

## Включаемые файлы (Includes)

Конвейер включает в себя следующий файл:

- `.helm.gitlab-ci.yml` из проекта `va1erify-gitlab-ci/helm` (версия `v1.0.0`)

## Переменные (Variables)

Конвейер использует следующие переменные:

- **package-upload-gitlab-package-registry-variables**: Переменные для загрузки Helm чартов в GitLab Package Registry.
  - `CHART_VERSION`: Версия чарта, устанавливается значением тега коммита.
  - `CHART_NAME`: Название проекта, используется для имени чарта.
  - `PROJECT_ID`: Идентификатор проекта в GitLab.
  - `FORM`: Формат данных для загрузки чарта.
  - `GITLAB_URL_TO_UPLOAD_HELM_CHART`: URL для загрузки чарта в GitLab Package Registry.
  - `GITLAB_USERNAME_TO_UPLOAD_HELM_CHART`: Логин пользователя GitLab.
  - `GITLAB_ACCESS_TOKEN_TO_UPLOAD_HELM_CHART`: Токен доступа GitLab для загрузки чарта.

## Правила (Rules)

- **build_rules**: Правила для этапа сборки. Задание выполняется всегда, если присутствует тег коммита.

## Скрипты (Scripts)

В конвейере используется скрипт `check-tag`, который выводит значение тега коммита:

- `echo $CI_COMMIT_TAG`

## Задания (Jobs)

Конвейер включает следующее задание:

1. **helm chart package and upload**: Пакетирование и загрузка Helm чартов в GitLab Package Registry.
   - Расширяет задание `.helm_package_upload_chart` из включаемого файла `.helm.gitlab-ci.yml`.
   - Выполняется на этапе `build`.
   - Использует правила `build_rules` для выполнения задания при наличии тега коммита.
   - Использует переменные `package-upload-gitlab-package-registry-variables`.
   - Перед выполнением основного скрипта выполняет скрипт `check-tag`.

## Использование

Чтобы запустить этот конвейер, создайте тег для коммита в репозитории. Конвейер автоматически выполнит задание пакетирования и загрузки Helm чарта в GitLab Package Registry.